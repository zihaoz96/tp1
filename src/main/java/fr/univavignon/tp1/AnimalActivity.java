package fr.univavignon.tp1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class AnimalActivity extends AppCompatActivity {

    private String name;

    private ImageView imageview;

    private TextView t1,t2,t3,t4,nameAnimal;

    private EditText editText;

    private Button enregistre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        nameAnimal = (TextView) findViewById(R.id.nameAnimal);
        editText = (EditText) findViewById(R.id.editText);
        enregistre = (Button) findViewById(R.id.button);
        imageview = (ImageView) findViewById(R.id.imgView);
        t1 = (TextView) findViewById(R.id.textView2);
        t2 = (TextView) findViewById(R.id.textView4);
        t3 = (TextView) findViewById(R.id.textView6);
        t4 = (TextView) findViewById(R.id.textView8);

        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        String imageFile = (String) AnimalList.getAnimal(name).getImgFile();
        int resId = getResource(imageFile);

        nameAnimal.setText(name);

        imageview.setImageResource(resId);

        t1.setText(Integer.toString(AnimalList.getAnimal(name).getHightestLifespan()) + " annees");
        t2.setText(Integer.toString(AnimalList.getAnimal(name).getGestationPeriod()) + " jours");
        t3.setText(Float.toString(AnimalList.getAnimal(name).getBirthWeight()) + " kg");
        t4.setText(Integer.toString(AnimalList.getAnimal(name).getAdultWeight()) + " kg");
        editText.setText(AnimalList.getAnimal(name).getConservationStatus());

        enregistre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimalList.getAnimal(name).setConservationStatus(editText.getText().toString());
                Toast.makeText(AnimalActivity.this, "Bien enregistre ! ", Toast.LENGTH_LONG).show();
            }
        });

    }

    public int  getResource(String imageName){
        Context ctx=getBaseContext();
        int resId = getResources().getIdentifier(imageName, "drawable", ctx.getPackageName());

        return resId;
    }

}
