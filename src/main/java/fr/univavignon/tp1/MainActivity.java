package fr.univavignon.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity {

    private String[] animaux = AnimalList.getNameArray();

    private ListView listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listview = (ListView) findViewById(R.id.listView);

        BaseAdapter adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return animaux.length;
            }

            @Override
            public Object getItem(int position) {
                return animaux[position];
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View layout = layout = View.inflate(MainActivity.this, R.layout.item_view, null);

                ImageView img = (ImageView) layout.findViewById(R.id.img);
                TextView name = (TextView) layout.findViewById(R.id.pig);

                img.setImageResource(getResource(AnimalList.getAnimal(animaux[position]).getImgFile()));
                name.setText(animaux[position]);


                return layout;
            }

        };
        listview.setAdapter(adapter);



        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final String item = (String) parent.getItemAtPosition(position);

                toAnimalActivity(item);
            }
        });
    }

    private void toAnimalActivity(String name){
        Intent intent = new Intent(MainActivity.this, AnimalActivity.class);
        intent.putExtra("name", name);
        startActivity(intent);
    }

    public int  getResource(String imageName){
        Context ctx=getBaseContext();
        int resId = getResources().getIdentifier(imageName, "drawable", ctx.getPackageName());

        return resId;
    }
}
